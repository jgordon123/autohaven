import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";

function SalespersonHistory() {
  const [individualSales, setIndividualSales] = useState([]);
  //   const [individuals, setIndividuals] = useState("");
  //   const [salespeoples, setSalespeoples] = useState([]);
  const [specifiedSalesperson, setSpecifiedSalesperson] = useState("");

  // All Sales History Data for each individual
  const salespersonHistoryData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setIndividualSales(data.sale);
    }
  };

  useEffect(() => {
    salespersonHistoryData();
  }, []);

  const handleSpecifiedSalespersonsChange = (event) => {
    setSpecifiedSalesperson(event.target.value);
  };

  const filteredSales = individualSales.filter(
    (individual) => individual.salesperson.employee_id === specifiedSalesperson
  );

  return (
    <>
      <h1>Salesperson History</h1>

      <div className="mb-3">
        <label htmlFor="specified_Salesperson" className="form-label">
          Salesperson
        </label>
        <select
          value={specifiedSalesperson}
          onChange={handleSpecifiedSalespersonsChange}
          name="specified_Salesperson"
          required
          id="specified_Salesperson"
          className="form-select"
        >
          <option>Choose a salesperson</option>
          {
            individualSales
              .map(individual => individual.salesperson.employee_id)
              .filter((employeeId, index, array) => array.indexOf(employeeId) === index)
              .map(employeeId => {
                const individual = individualSales.find(item => item.salesperson.employee_id === employeeId);

                return individual && (
                  <option
                    key={individual.salesperson.id}
                    value={individual.salesperson.employee_id}
                    >
                    {`${individual.salesperson.first_name} ${individual.salesperson.last_name}`}
                    </option>
                );
              })
          }
        </select>
      </div>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {filteredSales.map((individual, index) => (
            <tr
              key={index}
              value={individual.salesperson.employee_id}
            >
              <td>
                {individual.salesperson.first_name +
                  " " +
                  individual.salesperson.last_name}
              </td>
              <td>
                {individual.customer.first_name +
                  " " +
                  individual.customer.last_name}
              </td>
              <td>{individual.automobile.vin}</td>
              <td>{individual.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default SalespersonHistory;
