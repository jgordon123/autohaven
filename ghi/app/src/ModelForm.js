import { useEffect, useState } from 'react';


function ModelForm() {

    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [picture, setPicture] = useState('');
    const [manufacturer, setManufacturer] = useState('');

    const getData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }

    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.picture_url = picture;
        data.manufacturer_id = manufacturer;

        // console.log(data)

        const modelUrl = 'http://localhost:8100/api/models/'
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
        },
    };
    const modelResponse = await fetch(modelUrl, fetchOptions);
    // console.log(modelResponse)
    if(modelResponse.ok){
        setName('');
        setPicture('');
        setManufacturer('');
    }
}

    useEffect(() => {
        getData();
    }, []);

    return(

        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a Vehicle Model</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={(e) => setName(e.target.value)} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={picture} onChange={(e) => setPicture(e.target.value)} placeholder="picture_url" required type="text" id="picture_url" name="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <label htmlFor="manufacturer" className="form-label">Manufacturer</label>
                    <select value={manufacturer} onChange={(e) => setManufacturer(e.target.value)} required id="manufacturer" name="manufacturer" className="form-select">
                        <option value="">Choose a Manufacturer</option>
                        {manufacturers.map(manufacturer => {
                            return (
                                <option value={ manufacturer.id } key={ manufacturer.id } >
                                    { manufacturer.name }
                                </option>
                            )
                        })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </>
    );
}

export default ModelForm;
