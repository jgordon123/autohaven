import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";



function AppointmentList() {

    const [appointments, setAppointments] = useState([]);


    const getData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);

        if(response.ok){
            const data = await response.json();
            // console.log(data)
            setAppointments(data.appointments);
        }
    }

    const cancelAppt = async (id) => {
        const data = {};
        data.status = "Canceled"

        const cancelUrl = `http://localhost:8080/api/appointments/${id}/cancel/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const cancelResponse = await fetch(cancelUrl, fetchConfig)

        if(cancelResponse.ok){
            getData();
        }
    };

    const finishAppt = async (id) => {
        const data = {};
        data.status = "Finished"

        const finishUrl = `http://localhost:8080/api/appointments/${id}/finish/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const finishResponse = await fetch(finishUrl, fetchConfig)

        if(finishResponse.ok){
            getData();
        }
    };



    useEffect(() => {
        getData();
    }, [])

    return(
        <>
        <h1>Appointment List</h1>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>VIP Service</th>
                    <th>Customer Name</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Reason</th>
                    <th>Technician</th>
                    <th>Update Status</th>
                </tr>
            </thead>
            <tbody>
                {appointments.filter(appointment => appointment.status.includes("Created")).map((filteredAppointment) => {
                    return(
                        <tr key={filteredAppointment.id}>
                            <td>{filteredAppointment.vin}</td>
                            <td>{(filteredAppointment.vip) ? 'Yes' : 'No'}</td>
                            <td>{filteredAppointment.customer}</td>
                            <td>{new Date(filteredAppointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(filteredAppointment.date_time).toLocaleTimeString()}</td>
                            <td>{filteredAppointment.reason}</td>
                            <td>{filteredAppointment.technician.employee_id}</td>
                            <td>
                                <button className="btn btn-danger" onClick={() => cancelAppt(filteredAppointment.id)}>Cancel</button>
                            </td>
                            <td>
                            <button className="btn btn-success" onClick={() => finishAppt(filteredAppointment.id)}>Finish</button>

                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <Link to="/appointments/create">
          <button className="btn btn-info">Schedule Appointment</button>
        </Link>
        </>

    );
}

export default AppointmentList
