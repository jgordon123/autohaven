import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";



function SalespeopleList() {
  const [salespersons, setSalespersons] = useState([]);

  // Working on the delete button included in the jsx near bottom of the page

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/salespeople/");

    if (response.ok) {
      const data = await response.json();
      setSalespersons(data.salesperson);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
    <h1>Sales Team</h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
        </tr>
      </thead>
      <tbody>
        {salespersons.map((salesperson) => {
          return (
            <tr key={salesperson.employee_id}>
              <td>{salesperson.employee_id}</td>
              <td>{salesperson.first_name}</td>
              <td>{salesperson.last_name}</td>
            </tr>
          );
        })}
      </tbody>
      </table>
      <div>
        <Link to="/salesperson/create">
          <button className="btn btn-info">Add a Salesperson</button>
        </Link>
      </div>
    </>
  );

}

export default SalespeopleList;
