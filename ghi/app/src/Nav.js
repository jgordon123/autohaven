import { NavLink, Link } from "react-router-dom";
import autohaven_logo from "./images/autohaven_logo.png";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <Link className ="navbar-brand" to="/">
          <img src={autohaven_logo} alt="AutoHaven Logo" style={{width: '150px' }} />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Inventory
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                    <li><Link className="dropdown-item" to="/manufacturers/create">Create Manufacturer</Link></li>
                    <li><Link className="dropdown-item" to="/manufacturers">Manufacturers List</Link></li>
                    <li><Link className="dropdown-item" to="/models/create">Create Model</Link></li>
                    <li><Link className="dropdown-item" to="/models">Models List</Link></li>
                    <li><Link className="dropdown-item" to="/automobiles/create">Create Automobile</Link></li>
                    <li><Link className="dropdown-item" to="/automobiles">Automobiles List</Link></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Services
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                  <li><Link className="dropdown-item" to="/appointments/create">Create Appointment</Link></li>
                  <li><Link className="dropdown-item" to="/appointments">Appointment List</Link></li>
                  <li><Link className="dropdown-item" to="/appointments/history">Service History</Link></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Sales
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                    <li><Link className="dropdown-item" to="/customers/create">Create Customer</Link></li>
                    <li><Link className="dropdown-item" to="/customers">Customers List</Link></li>
                    <li><Link className="dropdown-item" to="/sales/create">Create Sale</Link></li>
                    <li><Link className="dropdown-item" to="/sales">Sales List</Link></li>
                    <li><Link className="dropdown-item" to="/sales/history">Sales Persons History</Link></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="collapse navbar-collapse" id="navbarNavDarkDropdown">
              <ul className="navbar-nav">
                <li className="nav-item dropdown">
                  <a className="nav-link dropdown-toggle" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Employees
                  </a>
                  <ul className="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                    <li><Link className="dropdown-item" to="/salesperson/create">Create Sales Person</Link></li>
                    <li><Link className="dropdown-item" to="/salespeople">Sales Team</Link></li>
                    <li><Link className="dropdown-item" to="/technicians/create">Create Technician</Link></li>
                    <li><Link className="dropdown-item" to="/technicians">Technicians</Link></li>
                  </ul>
                </li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
