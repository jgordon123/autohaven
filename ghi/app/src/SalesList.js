import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";

function SalesList() {
  const [allSales, setAllSales] = useState([]);

  const allSaleData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      // may have to change 'data.sale' to just 'data' to see whats there
      setAllSales(data.sale);
    }
  };

  useEffect(() => {
    allSaleData();
  }, []);


  return (
    <>
      <h1>Sales</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {allSales.map((allSale, index) => {
            return (
              <tr key={index}>
                <td>{allSale.salesperson.employee_id}</td>
                <td>
                  {allSale.salesperson.first_name +
                    " " +
                    allSale.salesperson.last_name}
                </td>
                <td>
                  {allSale.customer.first_name +
                    " " +
                    allSale.customer.last_name}
                </td>
                <td>{allSale.automobile.vin}</td>
                <td>{allSale.price}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Link to="/sales/create">
        <button className="btn btn-info">Add a Sale</button>
      </Link>
    </>
  );
}

export default SalesList;
