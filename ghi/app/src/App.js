import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianList from './TechList';
import TechForm from './TechForm';
import AppointmentList from './AppointmentList';
import ApptForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';
import SalespersonForm from "./SalespersonForm";
import SalespeopleList from "./SalespeopleList";
import CustomerForm from "./CustomerForm";
import CustomerList from "./CustomerList";
import SaleForm from "./SaleForm";
import SalesList from "./SalesList";
import SalespersonHistory from "./SalespersonHistory";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturerForm";
import ModelsList from "./ModelsList";
import ModelForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={ <MainPage /> } />
          <Route path="technicians" element={ <TechnicianList /> } />
          <Route path="technicians/create" element={ <TechForm /> } />
          <Route path="appointments/" element={ <AppointmentList /> } />
          <Route path="appointments/create" element={ <ApptForm /> } />
          <Route path="appointments/history" element={ <ServiceHistory /> } />
          <Route path="/" element={<MainPage />} />
          <Route path="salesperson/create" element={<SalespersonForm />} />
          <Route path="salespeople/" element={<SalespeopleList />} />
          <Route path="customers/create" element={<CustomerForm />} />
          <Route path="customers/" element={<CustomerList />} />
          <Route path="sales/create" element={<SaleForm />} />
          <Route path="sales/" element={<SalesList />} />
          <Route path="sales/history" element={<SalespersonHistory />} />
          <Route path="manufacturers/" element={<ManufacturersList />} />
          <Route path="manufacturers/create" element={<ManufacturerForm />} />
          <Route path="models" element={<ModelsList />} />
          <Route path="models/create" element={ <ModelForm /> } />
          <Route path="automobiles/" element={ <AutomobileList /> } />
          <Route path="automobiles/create" element={ <AutomobileForm /> } />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
