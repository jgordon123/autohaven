import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";


function AutomobileList() {
  const [automobiles, setAutomobiles] = useState([]);

  const getData = async () => {
    const url = "http://localhost:8100/api/automobiles/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      // console.log(data)
      setAutomobiles(data.autos);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <>
      <h1>Automobile Inventory</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Color</th>
            <th>Year</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Sold</th>
          </tr>
        </thead>
        <tbody>
          {/* This is where the map function will go */}
          {automobiles.map((auto) => {
            return (
              <tr key={auto.id}>
                <td>{auto.vin}</td>
                <td>{auto.color}</td>
                <td>{auto.year}</td>
                <td>{auto.model.manufacturer.name}</td>
                <td>{auto.model.name}</td>
                <td>{auto.sold ? "Yes" : "No"} </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Link to="/automobiles/create">
        <button className="btn btn-info">Add Automobile</button>
      </Link>
    </>
  );
}

export default AutomobileList;
