// import React from "react";
// import autohaven_hero from "./images/autohaven_hero.png";

// function MainPage() {
//   return (
//     <div className="px-4 py-5 my-5 text-center">
//       <img
//         src={autohaven_hero}
//         alt="AutoHaven Hero"
//         className="img-fluid"
//         style={{ height: "500px", maxWidth: "100%" }}
//       />
//       <h1 className="display-5 fw-bold">AutoHaven</h1>
//       <div className="col-lg-6 mx-auto">
//         <p className="lead mb-4">
//           The premiere solution for automobile dealership
//           management!
//         </p>
//       </div>
//     </div>
//   );
// }

// export default MainPage;

import React from "react";
import autohaven_hero from "./images/autohaven_hero.png";

function MainPage() {
  return (
    <div
      className="hero-section"
      style={{
        position: "relative",
        height: "100vh",
        overflow: "hidden",
        margin: 0,
      }}
    >
      <style>{`
        body {
          margin: 0;
          width: 100%;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
      <img
        src={autohaven_hero}
        alt="AutoHaven Hero"
        className="img-fluid"
        style={{
          width: "100%",
          height: "100%",
          objectFit: "cover",
          position: "fixed",
          top: 0,
          left: 0,
          zIndex: -1,
        }}
      />
      <div
        style={{
          position: "absolute",
          top: "11%",
          left: "50%",
          transform: "translateX(-50%)",
          color: "white",
          textAlign: "center",
        }}
      >
        <h1
          className="display-5 fw-bold"
          style={{ fontSize: "4rem", marginBottom: "1rem" }}
          >AutoHaven
        </h1>
        <div className="col-lg-6 mx-auto">
          <p
            className="lead mb-4"
            style={{ fontSize: "1.5rem", fontWeight: "normal",  }}
          >
            The premiere solution for automobile dealership management!
          </p>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
