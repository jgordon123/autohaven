from django.urls import path
from .views import api_list_appointments, api_delete_appointment, api_update_appointment, api_list_techs, api_delete_tech

urlpatterns = [
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:id>/", api_delete_appointment, name="api_delete_appointment"),
    path("appointments/<int:id>/cancel/", api_update_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish/", api_update_appointment, name="finish_appointment"),
    path("technicians/", api_list_techs, name="list_technicians"),
    path("technicians/<int:id>/", api_delete_tech, name="delete_tech")
]
