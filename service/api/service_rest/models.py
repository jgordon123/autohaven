from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150)

class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False, null=True)
    reason = models.CharField(max_length=150)
    status = models.CharField(max_length=150, default="Created")
    vin = models.CharField(max_length=17, unique=True)
    customer = models.CharField(max_length=150)
    vip = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"vin": self.vin})
