from django.urls import path

from .api_views import (
    api_list_salesperson, api_list_customer, api_list_sale, api_show_sale, api_show_customer, api_show_salesperson
)

urlpatterns = [
    #create paths for imported views
    path("salespeople/", api_list_salesperson, name="api_create_salesperson"),
    path("salespeople/<int:id>/", api_show_salesperson, name="api_show_salesperson"),
    path("customers/", api_list_customer, name="api_create_customer"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path("sales/", api_list_sale, name="api_create_sale"),
    path("sales/<int:id>/", api_show_sale, name="api_show_sale"),
]
