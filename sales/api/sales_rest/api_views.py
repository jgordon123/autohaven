from django.http import JsonResponse
import json

from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, Salesperson, Customer, Sale

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id"]

class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]

class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":

        salesperson = Salesperson.objects.all()

        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, id):
    if request.method == "GET":
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                salesperson,
                encoder=SalespersonDetailEncoder,
                safe=False
            )
    elif request.method == "DELETE":
            count, _ = Salesperson.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_customer(request):
    if request.method == "GET":

        customer = Customer.objects.all()

        return JsonResponse(
            {"customer": customer},
            encoder=CustomerDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, id):
    if request.method == "GET":
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerDetailEncoder,
                safe=False
            )
    elif request.method == "DELETE":
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})




@require_http_methods(["GET", "POST"])
def api_list_sale(request):
    if request.method == "GET":

        sale = Sale.objects.all()

        # CHECKING POLLER - COMMENT OUT 2 LINES BELOW
        automobiles = AutomobileVO.objects.all()
        print(automobiles.values())
        print("LOOK HERE")
        #CHECKING POLLER - COMMENT OUT 2 LINES ABOVE


        return JsonResponse(
            {"sale": sale},
            encoder=SaleDetailEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            #-- Try Automobile ForeignKey
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
            #-- Try Salesperson ForeignKey
            # idk if Salesperson needs to have a VO or if I do .get(all)
            salesperson_id=content["salesperson"]
            salesperson = Salesperson.objects.get(employee_id=salesperson_id)
            content["salesperson"] = salesperson
            #-- Try Customer ForeignKey
            # idk if Customer needs to have a VO or if I do .get(all)
            # customer_id = content["customer"]
            customer = Customer.objects.get(id=content["customer"])
            content["customer"] = customer


        #-- Except Automobile ForeignKey
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile"},
                status=400,
            )
        #-- Except Salesperson ForeignKey
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson"},
                status=400,
            )
        #-- Except Customer ForeignKey
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer"},
                status=400,
            )

        # Create Sale info (all listed below)
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                sale,
                encoder=SaleDetailEncoder,
                safe=False
            )
    elif request.method == "DELETE":
            count, _ = Sale.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
